'use strict'
/* global Opal */

const hljs = require('highlight.js')

class HighlightSyntaxHighlighter {
  format (node, lang, opts = {}) {
    opts = Object.assign({}, opts, {
      transform: (x, code) => {
        code['$[]=']('class', `language-${lang || 'none'} hljs`)
      },
    })

    const toHash = function (object) {
      return object && !object.$$is_hash ? Opal.hash2(Object.keys(object), object) : object
    }
    return Opal.send(this, Opal.find_super_dispatcher(this, 'format', this.$format), [node, lang, toHash(opts)])
  }

  handlesHighlighting () { return true }

  highlight (node, source, lang, opts) {
    return lang
      ? hljs.highlight(source, { language: lang }).value
      : hljs.highlightAuto(source).value
  }
}

function doRegister (registry) {
  const AsciidoctorModule = registry.$$base_module
  const SyntaxHighlighterRegistry = AsciidoctorModule.$$.SyntaxHighlighter
  SyntaxHighlighterRegistry.register('highlight.js', HighlightSyntaxHighlighter)
}

module.exports.register = function (registry, config = {}) {
  if (typeof registry.register === 'function') {
    doRegister(registry)
  } else {
    const clazz = registry.$$class
    const extensionsModule = clazz.$$base_module
    doRegister(extensionsModule)
  }
  return registry
}
