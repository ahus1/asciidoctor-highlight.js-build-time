/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const highlighter = require('./../lib/asciidoctor-highlight.js-build-time')
const { expect } = require('chai')

describe('asciidoctor-highlight.js-build-time tests', () => {
  function load (text, registry) {
    highlighter.register(registry)
    const options = { attributes: { 'source-highlighter': 'highlight.js' } }
    return asciidoctor.load(text, options)
  }

  ;[
    [asciidoctor.Extensions, 'global'],
    [asciidoctor.Extensions.create(), 'registry'],
  ].forEach(([registry, name]) => {
    it(`highlighting happens (${name})`, () => {
      const doc = load(`
[source,js]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {
  handlesHighlighting () { return true }

  highlight (node, source, lang, opts) {
    return hljs.highlight(lang, source).value
  }
}
----
`, registry)
      const html = doc.convert()
      expect(html).to.contain('<code class="language-js hljs" data-lang="js">')
      expect(html).to.contain('<span class="hljs-keyword">return</span>')
    })

    it(`conums (${name})`, () => {
      const doc = load(`
[source,js]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {
  handlesHighlighting () { return true } //<1>

  highlight (node, source, lang, opts) {
    return hljs.highlight(lang, source).value
  }
}
----
`, registry)
      const html = doc.convert()
      expect(html).to.contain('//<b class="conum">(1)</b>')
    })

    it(`no language (${name})`, () => {
      const doc = load(`
[source]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {
  handlesHighlighting () { return true }

  highlight (node, source, lang, opts) {
    return hljs.highlight(lang, source).value
  }
}
----
`, registry)
      const html = doc.convert()
      expect(html).to.contain('<code class="language-none hljs">')
      expect(html).to.contain('<span class="hljs-keyword">return</span>')
    })
  })
})
