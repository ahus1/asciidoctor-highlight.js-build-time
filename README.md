# Asciidoctor asciidoctor-highlight.js-build-time Extension
:version: 0.0.3

`@djencks/asciidoctor-highlight.js-build-time` provides an Asciidoctor.js build-time SyntaxHighlighter using highlight.js.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-highlight.js-build-time/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/asciidoctor-highlight.js-build-time.

Currently available through a line like this in `package.json`:


    "@djencks/asciidoctor-highlight.js-build-time": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-asciidoctor-highlight.js-build-time-v0.0.2.tgz",


The project git repository is https://gitlab.com/djencks/asciidoctor-highlight.js-build-time

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-highlight.js-build-time/-/blob/master/README.adoc

## Usage in Antora
NOTE: this requires asciidoctor 2, which is not yet integrated into Antora.
see https://gitlab.com/djencks/asciidoctor-highlight.js-build-time/-/blob/master/README.adoc

## Antora Example project

An example project showing simple use of this extension is under asciidoctor-2-module in `https://gitlab.com/djencks/antora-extension-examples`.
